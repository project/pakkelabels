Pakkelabels.dk
==

Integration between Drupal Commerce and the Danish shipping service provider <a href="http://pakkelabels.dk">Pakkelabels.dk</a>.

<h3>Under active development</h3>

This module is being developed right now.

<h3>Requirements</h3>

<ul>
  <li><a href="http://drupal.org/project/libraries">libraries</a>
  <li><a href="https://github.com/discimport/pakkelabels-dk">Pakkelabels php-sdk</a>
</ul>

<h3>3. Party development and sponsors</h3>

This has been developed by a 3. party developer. If you need support, you should use the issue queue at <a href="http://drupal.org/project/pakkelabels">discimport.dk</a>. <a href="http://pakkelabels.dk">Pakkelabels.dk</a> is not able to provide support for this module.
The development has been sponsored by <a href="http://discimport.dk">discimport.dk</a>.
