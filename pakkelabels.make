api = 2
core = 7.x

libraries[pakkelabels][download][type] = "git"
libraries[pakkelabels][download][url] = "https://github.com/discimport/pakkelabels-dk.git"
libraries[pakkelabels][directory_name] = "pakkelabels-php"
libraries[pakkelabels][destination] = "libraries"
libraries[pakkelabels][branch] = "master"
libraries[pakkelabels][revision] = "bb41364"
